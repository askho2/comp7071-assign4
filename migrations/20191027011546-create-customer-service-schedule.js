'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("customer_service_schedule", {
      customerid: {
        type: Sequelize.INTEGER,
        references: {
          model: "customer",
          key: "id"
        }
      }, 
      service_type_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "service_type",
          key: "id"
        }
      },
      employee_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "employee",
          key: "id"
        }
      },
      start_date_time: Sequelize.DATE,
      actual_duration: Sequelize.INTEGER,
      status: {
        type: Sequelize.ENUM("READY", "INPROGRESS", "COMPLETED")
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("customer_service_schedule");
  }
};
