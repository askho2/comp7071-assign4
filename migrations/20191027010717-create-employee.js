'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("employee", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: Sequelize.TEXT,
      street: Sequelize.TEXT,
      city: Sequelize.TEXT,
      manager_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "employee",
          key: "id"
        },
        onUpdate: "cascade"
        // no action on delete.
      },
      job_title: Sequelize.TEXT,
      certification: Sequelize.TEXT,
      salary: Sequelize.DECIMAL(10, 2)
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("employee");
  }
};
