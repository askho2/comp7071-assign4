'use strict';
module.exports = (sequelize, DataTypes) => {
    const CustomerService = sequelize.define('CustomerService', {
        customerid: {
            type: DataTypes.INTEGER,
            references: {
              model: "customer",
              key: "id"
            }
          }, 
          service_type_id: {
            type: DataTypes.INTEGER,
            references: {
              model: "service_type",
              key: "id"
            }
          },
          specific_details: DataTypes.TEXT,
          expected_duration: DataTypes.INTEGER
    }, {
      tableName: "customer_service",
      timestamps: false
    });
    CustomerService.removeAttribute("id");
    CustomerService.associate = function(models) {
      models.CustomerService.belongsTo(models.Customer, {
        targetKey: "id",
        foreignKey: "id"
      });
      models.CustomerService.belongsTo(models.ServiceType, {
        targetKey: "id",
        foreignKey: "id"
      });
    };
    return CustomerService;
};