'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("service_type", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: Sequelize.TEXT,
      certification_rqts: Sequelize.TEXT,
      rate: Sequelize.DECIMAL(10, 2)
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('service_type');
  }
};
