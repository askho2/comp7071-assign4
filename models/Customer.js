'use strict';
module.exports = (sequelize, DataTypes) => {
  const Customer = sequelize.define('Customer', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: DataTypes.TEXT,
    street: DataTypes.TEXT,
    city: DataTypes.TEXT,
    birthdate: DataTypes.DATEONLY,
    picture: DataTypes.BLOB,
    gender: DataTypes.ENUM("M","F")
  }, {
    tableName: "customer",
    timestamps: false
  });
  Customer.associate = function(models) {
    models.Customer.hasMany(models.CustomerService, {
      sourceKey: "id",
      foreignKey: "customerid"
    });
    models.Customer.hasMany(models.CustomerServiceSchedule, {
      sourceKey: "id",
      foreignKey: "customerid"
    });
  };
  return Customer;
};