'use strict';
module.exports = (sequelize, DataTypes) => {
	const ServiceType = sequelize.define('ServiceType', {
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		name: DataTypes.TEXT,
		certification_rqts: DataTypes.TEXT,
		rate: DataTypes.DECIMAL(10, 2)
	}, {
		tableName: "service_type",
		timestamps: false
	});
	ServiceType.associate = function(models) {
		models.ServiceType.hasMany(models.CustomerService, {
			sourceKey: "id",
			foreignKey: "service_type_id"
		});
		models.ServiceType.hasMany(models.CustomerServiceSchedule, {
			sourceKey: "id",
			foreignKey: "service_type_id"
		});
	};
	return ServiceType;
};