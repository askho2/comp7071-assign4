'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("customer_service", {
      customerid: {
        type: Sequelize.INTEGER,
        references: {
          model: "customer",
          key: "id"
        }
      }, 
      service_type_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "service_type",
          key: "id"
        }
      },
      specific_details: Sequelize.TEXT,
      expected_duration: Sequelize.INTEGER
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("customer_service");
  }
};
