'use strict';
module.exports = (sequelize, DataTypes) => {
    const Employee = sequelize.define('Employee', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: DataTypes.TEXT,
        street: DataTypes.TEXT,
        city: DataTypes.TEXT,
        manager_id: {
            type: DataTypes.INTEGER,
            references: {
            model: "employee",
            key: "id"
            },
            onUpdate: "cascade"
            // no action on delete.
        },
        job_title: DataTypes.TEXT,
        certification: DataTypes.TEXT,
        salary: DataTypes.DECIMAL(10, 2)
    }, {
        tableName: "employee",
        timestamps: false
    });
    Employee.associate = function(models) {
        models.Employee.hasMany(models.CustomerServiceSchedule, {
			sourceKey: "id",
			foreignKey: "employee_id"
        });
        models.Employee.belongsTo(models.Employee, {
            foreignKey: "manager_id",
            targetKey: "id"
        })
    };
    return Employee;
};