const models = require("./models/index");
const faker = require("faker");
const fs = require("fs");
const CUSTOMER_COUNT = 100000;
const EMPLOYEE_COUNT = 10;
const SERVICE_TYPE_COUNT = 10;
const CUSTOMER_SERVICE_COUNT = 100000;
const CUSTOMER_SERVICE_SCHEDULE_COUNT = 100;

const CERTIFICATIONS = [
    "MSC",
    "BSC",
    "BENG",
    "Food Safe",
    "GED",
    "LPN",
    "RN",
    "PHD",
    "MD"
];

function createEmployee() {
    return {
        name: `${faker.name.firstName()} ${faker.name.lastName()}`,
        street: faker.address.streetAddress(true),
        city: faker.address.city(),
        job_title: faker.name.jobTitle(),
        certification: faker.random.arrayElement(CERTIFICATIONS),
        salary: faker.random.number(90000)
    }
}

function createCustomer() {
    return {
        name: `${faker.name.firstName()} ${faker.name.lastName()}`,
        street: faker.address.streetAddress(true),
        city: faker.address.city(),
        birthdate: faker.date.past(25, new Date()),
        // picture: null,
        gender: faker.random.arrayElement(["M", "F"])
    }
}

function createServiceType() {
    return {
        name: faker.name.jobDescriptor(),
        certification_rqts: faker.random.arrayElement(CERTIFICATIONS),
        rate: faker.random.number({
            max: 1000,
            min: 10
        })
    }
}

function createCustomerService({customerID, serviceTypeID}) {
    return {
        customerid: customerID,
        service_type_id: serviceTypeID,
        specific_details: faker.lorem.paragraphs(40),
        expected_duration: faker.random.number({
            min: 1,
            max: 24
        })
    }
}

function createCustomerServiceSchedule({customerID, serviceTypeID, employeeID}) {
    return {
        customerid: customerID,
        service_type_id: serviceTypeID,
        employee_id: employeeID,
        start_date_time: faker.date.future(1, new Date()),
        actual_duration: faker.random.number({min: 0, max: 20}),
        status: faker.random.arrayElement([
            "READY",
            "INPROGRESS",
            "COMPLETED"
        ])
    }
}

async function main() {
    const image = await fs.promises.readFile("./500KB.jpg");
    const employees = [];
    const customers = [];
    const serviceTypes = [];
    const customerServices = [];
    const customerServiceSchedule = [];
    for (let i = 0; i < CUSTOMER_COUNT; i++) {
        const customer = createCustomer();
        // customer.picture = image;
        customers.push(customer);
    }
    for (let i = 0; i < EMPLOYEE_COUNT; i++) {
        employees.push(createEmployee());
    }
    for (let i = 0; i < SERVICE_TYPE_COUNT; i++) {
        serviceTypes.push(createServiceType());
    }
    console.log("Completed created customers, employees, and service types");
    const transaction1 = await models.sequelize.transaction()
    let createdEmployees, createdCustomers, createdServiceTypes    
    try {
        const [c1, c3] = await Promise.all([
            models.Employee.bulkCreate(employees, {transaction: transaction1}),
            models.ServiceType.bulkCreate(serviceTypes, {transaction: transaction1})
        ]);
        await transaction1.commit();
        createdCustomers = []; 
        createdEmployees = c1, createdServiceTypes = c3;
        const PAGE_SIZE = 50;
        for (let i = 0; i < (customers.length / PAGE_SIZE); i++) {
            const customerPage = customers.slice(0 + (i * PAGE_SIZE), 0 + (i * PAGE_SIZE) + PAGE_SIZE);
            const result = await models.Customer.bulkCreate(customerPage);
            createdCustomers.push(...result);
            console.log(`Completed inserting customer page ${i} of ${customers.length / PAGE_SIZE}`)
        } 
        console.log("Finished inserting employees, customers, service types");
    }
    catch (err) {
        await transaction1.rollback();
        throw err;
    }
    const employeeIDs = createdEmployees.map(emp => emp.id);
    const customerIDs = createdCustomers.map(cust => cust.id);
    const serviceTypeIDs = createdServiceTypes.map(serv => serv.id);
    const PAGE_COUNT = 5000;
    const transaction2 = await models.sequelize.transaction();
    try {
        for (let i = 0; i < CUSTOMER_SERVICE_COUNT / PAGE_COUNT; i++) {
            const records = [];
            for (let i = 0; i < PAGE_COUNT; i++) { // Batch the runs by page so we don't eat up all the memory trying to write half a million rows at a time.
                records.push(createCustomerService({
                    customerID: faker.random.arrayElement(customerIDs),
                    serviceTypeID: faker.random.arrayElement(serviceTypeIDs)
                }));    
            }
            await models.CustomerService.bulkCreate(records, { transaction: transaction2});
            console.log(`Wrote customer service record page ${i} of ${CUSTOMER_SERVICE_COUNT / PAGE_COUNT}`);
        }
        for (let i = 0; i < CUSTOMER_SERVICE_SCHEDULE_COUNT; i++) {
            await models.CustomerServiceSchedule.create(createCustomerServiceSchedule({
                customerID: faker.random.arrayElement(customerIDs),
                serviceTypeID: faker.random.arrayElement(serviceTypeIDs),
                employeeID: faker.random.arrayElement(employeeIDs)
            }), { transaction: transaction2});
            console.log(`Wrote customer service schedule record ${i} of ${CUSTOMER_SERVICE_SCHEDULE_COUNT}`);
        }
        await transaction2.commit();
    }
    catch (err) {
        await transaction2.rollback();
        throw err;
    }

    console.log("Finished inserting data");
    const now = new Date();
    const result = await models.sequelize.query(`
        SELECT customer.name, service_type_id, specific_details, expected_duration
        FROM customer
        INNER JOIN customer_service ON customer.id = customer_service.customerid
    `, {
        type: models.sequelize.QueryTypes.SELECT
    });
    console.log(`${(new Date()) - now }ms - ${result.length} rows returned`);
    process.exit()
}

main();