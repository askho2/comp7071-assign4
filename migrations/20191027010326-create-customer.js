'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('customer', {
      id: {
        type: Sequelize.INTEGER,
        autoIncrement: true
      },
      name: Sequelize.TEXT,
      street: Sequelize.TEXT,
      city: Sequelize.TEXT,
      birthdate: Sequelize.DATEONLY,
      picture: Sequelize.BLOB,
      gender: Sequelize.ENUM("M", "F")
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('customers');
  }
};