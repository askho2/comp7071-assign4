'use strict';
module.exports = (sequelize, DataTypes) => {
    const CustomerServiceSchedule = sequelize.define('CustomerServiceSchedule', {
        customerid: {
            type: DataTypes.INTEGER,
            references: {
              model: "customer",
              key: "id"
            }
        }, 
        service_type_id: {
            type: DataTypes.INTEGER,
            references: {
                model: "service_type",
                key: "id"
            }
        },
        employee_id: {
            type: DataTypes.INTEGER,
            references: {
                model: "employee",
                key: "id"
            }
        },
        start_date_time: DataTypes.DATE,
        actual_duration: DataTypes.INTEGER,
        status: {
            type: DataTypes.ENUM("READY", "INPROGRESS", "COMPLETED")
        }
    }, {
        tableName:"customer_service_schedule",
        timestamps: false
    });
    CustomerServiceSchedule.removeAttribute("id");
    CustomerServiceSchedule.associate = function(models) {
        models.CustomerServiceSchedule.belongsTo(models.Customer, {
            foreignKey: "customerid"
        });
        models.CustomerServiceSchedule.belongsTo(models.ServiceType, {
            foreignKey: "service_type_id"
        });
        models.CustomerServiceSchedule.belongsTo(models.Employee, {
            foreignKey: "employee_id"
        });

    };
    return CustomerServiceSchedule;
};